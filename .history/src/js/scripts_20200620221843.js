$('.read-more-content').addClass('hide')
$('.read-more-show, .read-more-hide').removeClass('hide')

$('.read-more-show').on('click', function (e) {
    $(this).next('.read-more-content').removeClass('hide');
    $(this).addClass('hide');
    e.preventDefault();
});

$('.read-more-hide').on('click', function (e) {
    var p = $(this).parent('.read-more-content');
    p.addClass('hide');
    p.prev('.read-more-show').removeClass('hide');
    e.preventDefault();
});


var image = new Array();
image[0] = "img/slider/b1.png";
image[1] = "img/slider/b2.png";
image[2] = "img/slider/b3.png";


var size = image.length
var x = Math.floor(size*Math.random())
$('#image').attr('src', image[x]);